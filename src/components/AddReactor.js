import React, { Component } from "react";

class AddReactor extends Component {
    state = {
        name: ""
    };
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    onSubmit = e => {
        e.preventDefault();
        this.props.addReactor(this.state.name);
        this.setState({
            name: ""
        });
    };
    render() {
        return (
            <form className="form-container" onSubmit={this.onSubmit}>
                <input
                    type="text"
                    placeholder="Add Reactor..."
                    className="input-text"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChange}
                />
                <input type="submit" value="Submit" className="input-submit" />
            </form>
        );
    }
}

export default AddReactor;
