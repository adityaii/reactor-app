import React from "react";
import ReactorItem from "./ReactorItem";

class Reactors extends React.Component {
    render() {
        return (
            <div>
                <table>
                    <thead><tr><td>Name</td><td>Active</td></tr></thead>
                    {this.props.reactors.map(reactor => (
                        <ReactorItem key={reactor.id} reactor={reactor} handleChange={this.props.handleChange} deleteReactor={this.props.deleteReactor}></ReactorItem>
                    ))}
                </table>
            </div >
        );
    }
}
export default Reactors;
