import React from "react";
import Reactors from "./Reactors";
import Header from "./layout/Header.js";
import AddReactor from "./AddReactor";
import { v4 as uuidv4 } from 'uuid';

class ReactorApp extends React.Component {
    state = {
        reactors: [
            {
                id: uuidv4,
                name: "Reactor 1",
                active: true
            },
            {
                id: uuidv4,
                name: "Reactor 2",
                active: false
            },
            {
                id: uuidv4,
                name: "Reactor 3",
                active: false
            }
        ]
    }
    handleChange = id => {
        console.log("clicked", id);
        this.setState({
            reactors: this.state.reactors.map(reactor => {
                if (reactor.id === id) {
                    reactor.active = !reactor.active;
                }
                return reactor;
            })
        });
    };
    deleteReactor = id => {
        console.log("deleted ", id);
        this.setState({
            reactors: [
                ...this.state.reactors.filter(reactor => {
                    return reactor.id !== id;
                })
            ]
        });
    };
    render() {
        return (
            <div>
                <Header />
                <AddReactor />
                <Reactors reactors={this.state.reactors} handleChange={this.handleChange} deleteReactor={this.deleteReactor} />
            </div>
        );
    }
}

export default ReactorApp;
