import React from "react";

const Header = () => {
    const headerStyle = {
        backgroundColor: "#66aaff",
        color: "#fff",
        padding: "10px 15px"
    }
    return (
        <header style={headerStyle}>
            <h1 style={{ fontSize: '25px', lineHeight: '2', margin: '0px' }}>Reactor App</h1>
        </header >
    )
}
export default Header;