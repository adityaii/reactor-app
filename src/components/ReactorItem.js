import React from "react";

class ReactorItem extends React.Component {
    render() {
        const completedStyle = {
            fontStyle: "italic",
            color: "#c5e2d2",
            textDecoration: "line-through"
        };
        const { active, id, name } = this.props.reactor;
        return (
            <div>
                <tr key={this.props.reactor.id}>
                    <td><input type="checkbox"
                        onChange={() => this.props.handleChange(id)}
                        checked={active} /></td>
                    <span style={active ? completedStyle : null}>
                        {name}
                    </span>
                    <td>
                        <button className="btn-style" onClick={() => this.props.deleteReactor(id)}> x </button>
                    </td>
                </tr>
            </div >
        );
    }
}
export default ReactorItem;