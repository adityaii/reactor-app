import React from 'react';
import ReactDOM from 'react-dom';

import ReactorApp from "./components/ReactorApp";

ReactDOM.render(
  <ReactorApp />,
  document.getElementById('root')
);
